# No.1  Monitoring resource dari komputer.
### * RAM
Perintah untuk melihat kondisi RAM total :
        
    free
![images](https://gitlab.com/alyakwardhani28/prak-so-alya-1217050008/-/raw/main/images/Screenshot_2023-05-11_100524.png)

Helper keyword untuk melihat kondisi RAM :

    free --help
![images](https://gitlab.com/alyakwardhani28/prak-so-alya-1217050008/-/raw/main/images/Screenshot_2023-05-11_101755.png)
c/ digunakan perintah **free --mega** untuk melihat kondisi RAM dalam satuan mega.

### * CPU
Perintah untuk monitoring proses yang berjalan pada CPU :

    ps -aux
![images](https://gitlab.com/alyakwardhani28/prak-so-alya-1217050008/-/raw/main/images/Screenshot_2023-05-11_105053.png)
Terlihat detail dari setiap prosesnya seperti Process Identification Number (PID), penggunaan resource, serta perintah inisiasinya. Kita juga bisa menggunakan **ps --help all** sbg helper perintah.

Perintah lain untuk monitoring CPU namun secara direct/langsung/berjalan :

    top
![images](https://gitlab.com/alyakwardhani28/prak-so-alya-1217050008/-/raw/main/images/Screenshot_2023-05-11_103641.png)

Kita dapat menggunakan perintah ini untuk monitoring berdasarkan PID/Commandnya.

    top | grep "<PID><ATAU><COMMAND"
![images](https://gitlab.com/alyakwardhani28/prak-so-alya-1217050008/-/raw/main/images/Screenshot_2023-05-11_110102.png)

Perintah untuk menghentikan program berdasarkan PID nya :
    
    kill <PID>
![images](https://gitlab.com/alyakwardhani28/prak-so-alya-1217050008/-/raw/main/images/Screenshot_2023-05-11_110958.png)
Gagal, karena kita tidak memiliki akses.

### * Hardisk
Perintah untuk mengecek statistik dari disk :
    
    df

Gunakan perintah helper untuk melihat deskripsi penggunaan **df** :

    df --help
![images](https://gitlab.com/alyakwardhani28/prak-so-alya-1217050008/-/raw/main/images/Screenshot_2023-05-11_111323.png)

# No.2 Manajemen program
### * Otomasi perintah dengan shellscript
Pertama, buat file shellscript :

    touch <namafile>.sh

Mengedit shellscript anda bisa menggunakan teks editor vim dengan cara :

    vim <namafile.sh>
![images](https://gitlab.com/alyakwardhani28/prak-so-alya-1217050008/-/raw/main/images/Screenshot_2023-05-11_144454.png)
Cara menggunakan vim :
1. klik **i** (insert) untuk edit
2. jika sudah di edit klik **esc** untuk mode view
3. klik **shift+: w** untuk save
4. klik **shift+: q** untuk keluar dari vim

Inisiasi shellscript :

    #!/bin/bash

Gunakan **echo** untuk mengisi file dgn teks dalam shellscript

    echo "<teks>"

Membuat parameter dalam shellscript :

    $1, $2, $3, dst

Membuat folder dalam shellscript :
    
    mkdir -p <parameter>

Membuat file dalam shellscript :

    touch <parameter>

Contoh program sederhana shellscript :
![images](https://gitlab.com/alyakwardhani28/prak-so-alya-1217050008/-/raw/main/images/Screenshot_2023-05-11_144042.png)

Menjalankan shellscript :
    
    ./<namafile.sh> <parameter1> <parameter2> <dst>

### * Penjadwalan eksekusi program dengan cron

Program crontab digunakan untuk menambahkan perintah penjadwalan berdasarkan aturan periode waktu tertentu, seperti per menit, per jam, per hari, dsb.

Anda dapat mengedit penjadwalan dengan perintah :

    crontab -e
Akan muncul teks editor vim lalu anda daapat membuat penjadwalan di sana

Untuk menambah perintah penjadwalan, anda dapat menggunakan :

    * * * * * touch <lokasifile>/<nama file yg ingin dibuat>.log
_ _ _ _ _
| | | | |
| | | | ----- Hari dalam seminggu (0 - 6) (0 adalah Minggu)
| | | ------- Bulan (1 - 12)
| | --------- Hari dalam sebulan (1 - 31)
| ----------- Jam (0 - 23)
------------- Menit (0 - 59)
c/ saya ingin menjadwalkan file iphone setiap jam 3 sore menit ke 57. Maka dapat ditulis sbg berikut.
![images](https://gitlab.com/alyakwardhani28/prak-so-alya-1217050008/-/raw/main/images/Screenshot_2023-05-11_155658.png)
Hasilnya akan ada file di folder tersebut setiap waktu yang ditentukan.

# No.3 Manajemen network
### a. Mengakses sistem operasi pada jaringan menggunakan SSH
Kita dapat menggunakan perintah berikut untuk konek ssh secara manual.

    ssh <user>@<host> (-p <port>)
perintah **-p <port>** itu optional, dipakai ketika kita ingin mengakses ssh dgn port yang diinginkan. Namun pada contoh dibawah ini tertera local host dengan port yang sudah ditetapkan.
![images](https://gitlab.com/alyakwardhani28/prak-so-alya-1217050008/-/raw/main/images/Screenshot_2023-05-11_171928.png)

### b. Mampu memonitor program yang menggunakan network
Kita dapat menggunkana perintah berikut untuk memonitor program yang menggunakan jaringan.

    netstat

Gunakan helper untuk menampilkan deskripsi penggunaan dari perintah **netstat**

    netstat --help

c/ Disini saya menggunakan perintah **netstat -tulpn** untuk menampilkan informasi tentang koneksi jaringan aktif, port yang digunakan, serta proses yang sedang mendengarkan atau mengeksekusi koneksi tersebut. 

Anda bisa menggunakan "grep" seperti pada monitoring proses untuk menampilkan status jaringan pada koneksi yang diinginkan. 
![images](https://gitlab.com/alyakwardhani28/prak-so-alya-1217050008/-/raw/main/images/Screenshot_2023-05-11_184828.png)

### c. Mampu mengoperasikan HTTP client
Kita dapat menggunakan **curl** untuk mengakses data dari URL dengan protokol HTTP, HTTPS, FTP, dan lainnya.

    curl <link>
![images](https://gitlab.com/alyakwardhani28/prak-so-alya-1217050008/-/raw/main/images/Screenshot_2023-05-11_185803.png)
Perintah ini hanya mengakses saja.

Selain **curl**, kita juga dapat menjalankan perintah berikut untuk mendownload url.

    wget <link>
![images](https://gitlab.com/alyakwardhani28/prak-so-alya-1217050008/-/raw/main/images/Screenshot_2023-05-11_185533.png)
Terlihat ada file 'index.html' yg terunduh

# No.4 Manajemen file dan folder
### a. Navigasi
Pindah Directory

    cd <namadirectory>

Kembali ke directory sebelumnya

    cd ..

Melihat lokasi yang ditempati

    pwd

### b. Manajemen file dan folder
Membuat directory/folder :

    mkdir <namadirectory>

Membuat file :

    touch <namafile>

Mengubah nama file :

    mv <namafile/directory sebelum> <namafile/directory sesudah>

Melihat isi directory

    ls

Melihat isi file 

    cat <namafile>

Menghapus directory

    rmdir <namadirectory>

Menghapus file

    rm <namafile> -rf

![images](https://gitlab.com/alyakwardhani28/prak-so-alya-1217050008/-/raw/main/images/Screenshot_2023-05-11_190335.png)

Untuk mengedit isi file anda bisa menggunakan **vim**

    vim <namafile>
Cara menggunakan vim :
1. klik **i** (insert) untuk edit
2. jika sudah di edit klik **esc** untuk mode view
3. klik **shift+: w** untuk save
4. klik **shift+: q** untuk keluar dari vim

### c. Manajemen ownership dan akses file dan folder
Untuk melihat permission dari tiap file gunakan perintah :

    ls -l
d/- itu menandakan jenis directory/file, setelahnya 3 digit pertama itu untuk owner, 3 digit kedua group dan 3 digit terakhir itu world
r = read
w = write
x = execute
-rwx------ owner dapat read, write dan execute file
dr---w---x owner dapat read, group dapat write dan world dapat execute directory

Untuk mengubah permission gunakan :

    chmod ___ <nama file>
 
 read = 4
 write = 2
 execute = 1
 ___ = owner group world
 c/ ingin mengizinkan owner rwx, group r, world x maka **chmod 741 <namafile>*** (7 = 4+2+1)
 ![images](https://gitlab.com/alyakwardhani28/prak-so-alya-1217050008/-/raw/main/images/Screenshot_2023-05-11_195655.png)

Untuk mengganti ownership dari directory/file:

    sudo chown <owner>:<group> <namafile/folder>
![images](https://gitlab.com/alyakwardhani28/prak-so-alya-1217050008/-/raw/main/images/Screenshot_2023-05-11_193212.png)
c/file castle ownershipnya tidak diubah dan group ownershipnya diubah menjadi suzuki. Namun, kita tidak dapat mengubahnya karena kita bukan super user.

### d. Pencarian Directory dan file
Untuk mencari sebuah file dan Directory gunakan :

    find <namafile/dir>
![images](https://gitlab.com/alyakwardhani28/prak-so-alya-1217050008/-/raw/main/images/Screenshot_2023-05-11_190621.png)
### e. Kompresi data
Untuk mengkompress data kita bisa gunakan :

    gzip <namafile/folder>

dan untuk unkompress :

    gunzip <namafile/folder>
![images](https://gitlab.com/alyakwardhani28/prak-so-alya-1217050008/-/raw/main/images/Screenshot_2023-05-11_192447.png)

Selain itu kita juga bisa menggunakan :

    tar -cf <namafile/dir.tar> <filesebelum>

dan undo kompress dengan :

    tar -xf <namafile/di.tar>
![images](https://gitlab.com/alyakwardhani28/prak-so-alya-1217050008/-/raw/main/images/Screenshot_2023-05-11_192956.png)
Perbedaannya kalau gzip itu file/foldernya keubah jadi bentuk zip semua, kalau yang tar itu file lamanya tidak ikut terubah jadi ada 2.

### 5.Mampu membuat program berbasis CLI menggunakan shell script
Inisiasi shellscript :

    #!/bin/bash

Gunakan **echo** untuk mengisi file dgn teks dalam shellscript

    echo "<teks>"

Membuat parameter dalam shellscript :

    $1, $2, $3, dst

Membuat folder dalam shellscript :
    
    mkdir -p <parameter>

Membuat file dalam shellscript :

    touch <parameter>
![images](https://gitlab.com/alyakwardhani28/prak-so-alya-1217050008/-/raw/main/images/Screenshot_2023-05-11_200124.png)

### 6. Upload link youtube
<https://youtu.be/m4ZPBj5ktn8>

### 7. Maze Game
<https://maze.informatika.digital/maze/liddle/>












